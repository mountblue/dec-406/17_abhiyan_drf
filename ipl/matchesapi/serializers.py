from rest_framework import serializers
from .models import Matches, Deliveries


class MatchesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Matches
        fields = ['id', 'city', 'date', 'team1', 'team2', 'winner']    


class DeliveriesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Deliveries
        fields = "__all__"   