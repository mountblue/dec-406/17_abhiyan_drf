
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from matchesapi import views

routers = routers.DefaultRouter()
routers.register(r'matches', views.MatchesViewSet)
routers.register(r'deliveries', views.DeliveriesViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(routers.urls)),
]
