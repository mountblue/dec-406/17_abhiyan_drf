from django.shortcuts import render
from rest_framework import viewsets
from .serializers import MatchesSerializer, DeliveriesSerializer
from .models import Matches, Deliveries


class MatchesViewSet(viewsets.ModelViewSet):
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer


class DeliveriesViewSet(viewsets.ModelViewSet):
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer
